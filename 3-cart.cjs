const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]

// Q1. Find all the items with price more than $65.

let highValueItems = Object.entries(products[0])
.reduce((accumulator,currentValue) => {

    if(Array.isArray(currentValue[1]) === false){

        let itemPrice = parseInt(currentValue[1].price.substring(1));

        if(itemPrice > 65){
            accumulator.push(currentValue[0]);
        }
    } else {

        let subHighValueItems = currentValue[1].reduce((subHighValueItem,currentSubHighValueItem) => {

            let item = Object.keys(currentSubHighValueItem);
            let itemPrice = parseInt(currentSubHighValueItem[item].price.substring(1));

            if(itemPrice > 65){
                subHighValueItem.push(item);
            }

            return subHighValueItem;
        },[]);

        accumulator.push(subHighValueItems);
    }    

    return accumulator;
},[]);

highValueItems = highValueItems.flat(2);

console.log(highValueItems);


// Q2. Find all the items where quantity ordered is more than 1.

let highQuantityItems = Object.entries(products[0])
.reduce((accumulator,currentValue) => {

    if(Array.isArray(currentValue[1]) === false){

        let itemQuantitity = currentValue[1].quantity;

        if(itemQuantitity > 1){
            accumulator.push(currentValue[0]);
        }
    } else {

        let subHighQuantityItems = currentValue[1].reduce((subHighQuantityItem,currentSubHighQuantityItem) => {

            let item = Object.keys(currentSubHighQuantityItem);
            let itemQuantitity = currentSubHighQuantityItem[item].quantity;

            if(itemQuantitity > 1){
                subHighQuantityItem.push(item);
            }

            return subHighQuantityItem;
        },[]);

        accumulator.push(subHighQuantityItems);
    }    

    return accumulator;
},[]);

highQuantityItems = highQuantityItems.flat(2);

console.log(highQuantityItems);


// Q.3 Get all items which are mentioned as fragile.

let fragileItems = Object.entries(products[0])
.reduce((accumulator,currentValue) => {

    if(Array.isArray(currentValue[1]) === false){

        if(currentValue[1].type !== undefined && currentValue[1].type === "fragile"){
            accumulator.push(currentValue[0]);
        }

    } else {

        let subFragileItems = currentValue[1].reduce((subFragileItem,currentFragileItem) => {
            
            let item = Object.keys(currentFragileItem);
            if(currentFragileItem[item].type !== undefined && currentFragileItem[item].type === "fragile"){
                subFragileItem.push(item);
            }

            return subFragileItem;
        },[]);

        accumulator.push(subFragileItems);
    }    

    return accumulator;
},[]);

fragileItems = fragileItems.flat(2);

console.log(fragileItems);

// Q.4 Find the least and the most expensive item for a single quantity.

let mostExpensiveItem = Object.entries(products[0])
.reduce((accumulator,currentValue) => {
   if(Array.isArray(currentValue[1]) === false){
    if(currentValue[1].quantity === 1){
        let itemPrice = parseInt(currentValue[1].price.substring(1));
        if(accumulator.length === 0){
            accumulator.push(currentValue[0],itemPrice);
        } else {
            if(itemPrice > accumulator[1]){
                accumulator.splice(0,2,currentValue[0],itemPrice);
            }
        }
    }
   } else {
    let subExpensiveItems = currentValue[1].reduce((subExpensiveItem,currentSubExpensiveItem) => {

        let item = Object.keys(currentSubExpensiveItem);
        let itemQuantitity = currentSubExpensiveItem[item].quantity;

        if(itemQuantitity === 1){
            let itemPrice = parseInt(currentSubExpensiveItem[item].price.substring(1));
            if(subExpensiveItem.length === 0){
                subExpensiveItem.push(item,itemPrice);
            } else {
                if(itemPrice > subExpensiveItem[1]){
                    subExpensiveItem.splice(0,2,item,itemPrice);
                }
            }
        }

        return subExpensiveItem;
    },[]);

    subExpensiveItems = subExpensiveItems.flat();
    if(accumulator.length === 0){
        accumulator.push(subExpensiveItems[0],subExpensiveItems[1]);
    } else {
        if(subExpensiveItems[1] > accumulator[1]){
            accumulator.splice(0,2,subExpensiveItems[0],subExpensiveItems[1]);
        }
    }

   }
   return accumulator;
},[]);


let leastExpensiveItem = Object.entries(products[0])
.reduce((accumulator,currentValue) => {
   if(Array.isArray(currentValue[1]) === false){
    if(currentValue[1].quantity === 1){
        let itemPrice = parseInt(currentValue[1].price.substring(1));
        if(accumulator.length === 0){
            accumulator.push(currentValue[0],itemPrice);
        } else {
            if(itemPrice < accumulator[1]){
                accumulator.splice(0,2,currentValue[0],itemPrice);
            }
        }
    }
   } else {
    let subLeastExpensiveItems = currentValue[1].reduce((subLeastExpensiveItem,currentLeastSubExpensiveItem) => {

        let item = Object.keys(currentLeastSubExpensiveItem);
        let itemQuantitity = currentLeastSubExpensiveItem[item].quantity;

        if(itemQuantitity === 1){
            let itemPrice = parseInt(currentLeastSubExpensiveItem[item].price.substring(1));
            if(subLeastExpensiveItem.length === 0){
                subLeastExpensiveItem.push(item,itemPrice);
            } else {
                if(itemPrice < subLeastExpensiveItem[1]){
                    subLeastExpensiveItem.splice(0,2,item,itemPrice);
                }
            }
        }

        return subLeastExpensiveItem;
    },[]);

    subLeastExpensiveItems = subLeastExpensiveItems.flat();
    if(accumulator.length === 0){
        accumulator.push(subLeastExpensiveItems[0],subLeastExpensiveItems[1]);
    } else {
        if(subLeastExpensiveItems[1] < accumulator[1]){
            accumulator.splice(0,2,subLeastExpensiveItems[0],subLeastExpensiveItems[1]);
        }
    }

   }
   return accumulator;
},[]);

let leastAndMostExpensiveItems = [leastExpensiveItem[0],mostExpensiveItem[0]];
console.log(leastAndMostExpensiveItems);
