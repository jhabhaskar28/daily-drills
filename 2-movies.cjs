const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 


// Q1. Find all the movies with total earnings more than $500M. 

let hugeMovies = Object.keys(favouritesMovies).reduce((accumulator,currentvalue) => {

    let totalEarningsLength = favouritesMovies[currentvalue].totalEarnings.length;
    let totalEarningsInteger = parseInt(favouritesMovies[currentvalue].totalEarnings.substring(1,totalEarningsLength));

    if(totalEarningsInteger > 500){
        accumulator[currentvalue] = favouritesMovies[currentvalue];
    }

    return accumulator;
},{});

console.log(hugeMovies);

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

let hugeAndPopularMovies = Object.keys(hugeMovies).reduce((accumulator,currentvalue) => {

    if(hugeMovies[currentvalue].oscarNominations > 3){
        accumulator[currentvalue] = hugeMovies[currentvalue];
    }
    
    return accumulator;
},{});

console.log(hugeAndPopularMovies);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".

let leonardoMovies = Object.keys(favouritesMovies).reduce((accumulator,currentvalue) => {

    if(favouritesMovies[currentvalue].actors.includes("Leonardo Dicaprio")){
        accumulator[currentvalue] = favouritesMovies[currentvalue];
    }

    return accumulator;
},{});

console.log(leonardoMovies);

// Q.4 Sort movies (based on IMDB rating)
//         if IMDB ratings are same, compare totalEarning as the secondary metric.

let favouritesMoviesArray = Object.entries(favouritesMovies);

let sortedfavouriteMoviesArray = favouritesMoviesArray.sort((movie1,movie2) => {

    //[1] contains the movie details in array format
    if(movie2[1].imdbRating !== movie1[1].imdbRating){
        return movie2[1].imdbRating - movie1[1].imdbRating;
    } else {
        return parseInt(movie2[1].totalEarnings.slice(1,-1)) - parseInt(movie1[1].totalEarnings.slice(1,-1));
    }
});

console.log(Object.fromEntries(sortedfavouriteMoviesArray));

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//         drama > sci-fi > adventure > thriller > crime

// let favouritesMoviesWithGenreRanks = Object.keys(favouritesMovies)
// .map((movie) => {
   
// })