const persons = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]


//1. Find all people who are Agender

let agenderPeople = persons.filter((person) => {
    return person.gender === "Agender";
});

console.log(agenderPeople);


// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]

let newIPAddresses = persons.map((person) => {
    let newIpAddress = person.ip_address.split('.');
    return newIpAddress;
});

console.log(newIPAddresses);

// 3. Find the sum of all the second components of the ip addresses.

let sumOfSecondComponents = newIPAddresses.reduce(function(accumulator,currentValue){
    let secondComponent = parseInt(currentValue[1]);
    accumulator += secondComponent;   
    return accumulator;
},0);

console.log(sumOfSecondComponents);

// 4. Find the sum of all the fourth components of the ip addresses.

let sumOfFourthComponents = newIPAddresses.reduce(function(accumulator,currentValue){
    let fourthComponent = parseInt(currentValue[3]);
    accumulator += fourthComponent;   
    return accumulator;
},0);

console.log(sumOfFourthComponents);

// 5. Compute the full name of each person and store it in a new key (full_name or something) for each person.

let fullNameArray = persons.map((person) => {
    if(person.fullName === undefined){
        let fullName = person.first_name + " " + person.last_name;
        person.full_name = fullName;
        return fullName;
    }
});

console.log(fullNameArray);

// 6. Filter out all the .org emails

let orgEmails = persons.filter((person) => {
    return person.email.includes(".org");
})
.map((person) => {
    return person.email;
});

console.log(orgEmails);


// 7. Calculate how many .org, .au, .com emails are there

let specificEmails = persons.filter((person) => {
    if(person.email.includes(".com") || person.email.includes(".au") || person.email.includes(".org")){
        return true;
    }
});

console.log(specificEmails.length);

// 8. Sort the data in descending order of first name

persons.sort((person1,person2) => {
    if(person1.first_name < person2.first_name){
        return 1;
    } else {
        return -1;
    }
});

console.log(persons);