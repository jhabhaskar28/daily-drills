const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 


// 1. Get all items that are available 

let availableItems = items.filter((anItem) => {
    return anItem.available === true;
});

console.log("Available Items:");
console.log(availableItems);
console.log('\n');


// 2. Get all items containing only Vitamin C.

let onlyVitaminCItems = items.filter((anItem) => {
    return anItem.contains === "Vitamin C";
});

console.log("Only Vitamin C Items:");
console.log(onlyVitaminCItems);
console.log('\n');

// 3.Get all items containing Vitamin A.

let vitaminAItems = items.filter((anItem) => {
    return anItem.contains
    .includes("Vitamin A");
});

console.log("Vitamin A Items:");
console.log(vitaminAItems);
console.log('\n');

// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
        
//         and so on for all items and all Vitamins.

let listOfAllVitamins = items.reduce(function(accumulator,currentValue){
    let vitaminsInItem = currentValue.contains
    .split(", ");

    if(!accumulator.includes(vitaminsInItem)){
        accumulator.push(vitaminsInItem);
    }

    return accumulator;
},[])
.flat(2);

// console.log(newList);

let groupedItems = listOfAllVitamins.map((vitamin) => {
    return vitamin;
})
.reduce(function(accumulator,currentValue){
    accumulator[currentValue] = items.reduce(function(accumulator2,currentValue2) {

        if(currentValue2.contains.includes(currentValue)){
            accumulator2.push(currentValue2.name);
        }
    
        return accumulator2;
    },[]);
    
    return accumulator;
},{});

console.log("Grouped Items:");
console.log(groupedItems);
console.log('\n');

// 5. Sort items based on number of Vitamins they contain.

let itemsWithVitaminCount = items.map((anItem) => {

    let vitaminsArray = anItem.contains
    .split(",");
    let vitaminsArrayLength = vitaminsArray.length;

    anItem.vitamin_count = vitaminsArrayLength;

    return anItem;
});

itemsWithVitaminCount.sort((item1,item2) => {
    if(item1.vitamin_count < item2.vitamin_count){
        return 1;
    } else {
        return -1;
    }
});

let sortedItems = itemsWithVitaminCount.map((anItem) => {
    delete anItem.vitamin_count;

    return anItem;
});

console.log("Sorted items based on vitamin count:");
console.log(sortedItems);