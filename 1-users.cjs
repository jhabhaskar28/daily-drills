const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 



// Q1 Find all users who are interested in playing video games.

let usersArray = Object.entries(users);

let videoGameUsersArray = usersArray.filter(user => {
    let interestString = user[1].interests.join(" ");
    if(interestString.includes("Video Games")){
        return user;
    }
});

let videoGameUsersObject = Object.fromEntries(videoGameUsersArray);
console.log(videoGameUsersObject);
 

// Q2 Find all users staying in Germany.

let userArray = Object.entries(users);
let germanUsersArray = userArray.filter(user => {
    return user[1].nationality === 'Germany';
});

let germanUsers = Object.fromEntries(germanUsersArray);
console.log(germanUsers);


// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

let newUserArray = Object.entries(users);

let newUserDesignationArray = newUserArray.map(user => {
    if(user[1].desgination.includes("Senior") && user[1].desgination.includes("Developer")){
        user[1]["Rank"] = 1;
    } else if (user[1].desgination.includes("Developer")){
        user[1]["Rank"] = 2;
    } else {
        user[1]["Rank"] = 3;
    }

    return user;
});

newUserDesignationArray.sort((user1,user2)=>{
    if(user1[1].Rank > user2[1].Rank){
        return 1;
    } else if(user1[1].Rank < user2[1].Rank){
        return -1;
    } else {
        if(user1[1].age < user2[1].age){
            return 1;
        } else {
            return -1;
        }
    }
});

let updatedUserDesignationArray = newUserDesignationArray.map(user => {
    delete user[1].Rank;
    return user;
});

let newUserDesignationObject = Object.fromEntries(updatedUserDesignationArray);
console.log(newUserDesignationObject);

// Q4 Find all users with masters Degree.

let mastersUsersArray = userArray.filter(user => {
    return user[1].qualification
    .includes("Masters");
});

let mastersUsers = Object.fromEntries(mastersUsersArray);
console.log(mastersUsers);


// Q5 Group users based on their Programming language mentioned in their designation.

let groupedUsersObject = {};

groupedUsersObject["Javascript"] = userArray.reduce(function(accumulator,currentValue){
    if(currentValue[1].desgination.includes("Javascript")){
        accumulator[currentValue[0]] = currentValue[1];
    }
    return accumulator;
},{});

groupedUsersObject["Python"] = userArray.reduce(function(accumulator,currentValue){
    if(currentValue[1].desgination.includes("Python")){
        accumulator[currentValue[0]] = currentValue[1];
    }
    return accumulator;
},{});

groupedUsersObject["Golang"] = userArray.reduce(function(accumulator,currentValue){
    if(currentValue[1].desgination.includes("Golang")){
        accumulator[currentValue[0]] = currentValue[1];
    }
    return accumulator;
},{});

console.log(groupedUsersObject);