const cards = [{"id":1,"card_number":"5602221055053843723","card_type":"china-unionpay","issue_date":"5/25/2021","salt":"x6ZHoS0t9vIU","phone":"339-555-5239"},
{"id":2,"card_number":"3547469136425635","card_type":"jcb","issue_date":"12/18/2021","salt":"FVOUIk","phone":"847-313-1289"},
{"id":3,"card_number":"5610480363247475108","card_type":"china-unionpay","issue_date":"5/7/2021","salt":"jBQThr","phone":"348-326-7873"},
{"id":4,"card_number":"374283660946674","card_type":"americanexpress","issue_date":"1/13/2021","salt":"n25JXsxzYr","phone":"599-331-8099"},
{"id":5,"card_number":"67090853951061268","card_type":"laser","issue_date":"3/18/2021","salt":"Yy5rjSJw","phone":"850-191-9906"},
{"id":6,"card_number":"560221984712769463","card_type":"china-unionpay","issue_date":"6/29/2021","salt":"VyyrJbUhV60","phone":"683-417-5044"},
{"id":7,"card_number":"3589433562357794","card_type":"jcb","issue_date":"11/16/2021","salt":"9M3zon","phone":"634-798-7829"},
{"id":8,"card_number":"5602255897698404","card_type":"china-unionpay","issue_date":"1/1/2021","salt":"YIMQMW","phone":"228-796-2347"},
{"id":9,"card_number":"3534352248361143","card_type":"jcb","issue_date":"4/28/2021","salt":"zj8NhPuUe4I","phone":"228-796-2347"},
{"id":10,"card_number":"4026933464803521","card_type":"visa-electron","issue_date":"10/1/2021","salt":"cAsGiHMFTPU","phone":"372-887-5974"}]

/* 

    1. Find all card numbers whose sum of all the even position digits is odd.
    2. Find all cards that were issued before June.
    3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/

// 1. Find all card numbers whose sum of all the even position digits is odd.

let oddSumCards = cards.map((card) => {

    let cardNumber = card.card_number;
    let cardNumberArray = cardNumber.split('');
    let evenFlag = 1;
    let evenPositionSum = cardNumberArray.reduce((accumulator,currentValue) => {
        if(evenFlag%2 == 0){
            accumulator += parseInt(currentValue);
        }
        evenFlag ++;

        return accumulator;
    },0);
    card["evenPositionSum"] = evenPositionSum;

    return card;
})
.filter((card) => {
    return card["evenPositionSum"]%2 !== 0;
});

console.log(oddSumCards);

// 2. Find all cards that were issued before June.

let cardsBeforeJune = cards.filter((card) => {

    let issueDateArray = card.issue_date.split("/");
    let issueMonth = parseInt(issueDateArray[0]);

    return issueMonth<6;
});

console.log(cardsBeforeJune);

// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.

let cardsWithCVV = cards.map((card) => {

    let randomNumer = Math.floor(Math.random()*10 + 1)*100 + Math.floor(Math.random()*10)*10 + Math.floor(Math.random()*10);
    card["CVV"] = randomNumer;

    return card;
});

console.log(cardsWithCVV);


// 4. Add a new field to each card to indicate if the card is valid or not.

let cardsWithValidity = cards.map((card) => {

    card["Valid"] = "Yes";

    return card;
});

console.log(cardsWithValidity);


// 5. Invalidate all cards issued before March.

let validCards = cards.map((card) => {

    let issueDateArray = card.issue_date.split("/");
    let issueMonth = parseInt(issueDateArray[0]);
    if(issueMonth<3){
        card["Valid"] = "No";
    }

    return card;
});

console.log(validCards);

// 6. Sort the data into ascending order of issue date.

let cardsWithIssueDateArray = cards.map((card) => {

    let issueDateArray = card.issue_date.split("/");
    card["IssueArray"] = issueDateArray;

    return card;
});

let sortedCards = cardsWithIssueDateArray.sort((card1,card2) => {

    if(card1.IssueArray[2] !== card2.IssueArray[2]){
        return parseInt(card1.IssueArray[2]) - parseInt(card2.IssueArray[2]);
    } else if (card1.IssueArray[0] !== card2.IssueArray[0]){
        return parseInt(card1.IssueArray[0]) - parseInt(card2.IssueArray[0]);
    } else {
        return parseInt(card1.IssueArray[1]) - parseInt(card2.IssueArray[1]);
    }
})
.map((sortedCard) => {

    delete sortedCard["IssueArray"];

    return sortedCard;
})


console.log(sortedCards);

// 7. Group the data in such a way that we can identify which cards were assigned in which months.

let monthsDictionary = ["January","February","March","April","May","June","July","August","September","October","November","December"];

let groupedCards = cards.reduce((accumulator,currentValue) => {

    let issueDateArray = currentValue.issue_date.split("/");
    let issueMonthNumber = parseInt(issueDateArray[0]);
    let issueMonth = monthsDictionary[issueMonthNumber-1];

    if(accumulator[issueMonth] === undefined){
        accumulator[issueMonth] = [currentValue];
    } else {
        accumulator[issueMonth].push(currentValue);
    }
    
    return accumulator;
},{});

console.log(groupedCards);