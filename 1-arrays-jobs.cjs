let persons = [{"id":1,"first_name":"Gregg","last_name":"Lacey","job":"Web Developer III","salary":"$3.62","location":"Malta"},
{"id":2,"first_name":"Hernando","last_name":"Scargill","job":"Sales Associate","salary":"$4.16","location":"France"},
{"id":3,"first_name":"Loren","last_name":"Doull","job":"Physical Therapy Assistant","salary":"$4.34","location":"U.S. Virgin Islands"},
{"id":4,"first_name":"Sherwood","last_name":"Adriano","job":"Web Developer II","salary":"$6.46","location":"Brazil"},
{"id":5,"first_name":"Addison","last_name":"Dofty","job":"Structural Engineer","salary":"$6.40","location":"Costa Rica"},
{"id":6,"first_name":"Jess","last_name":"McUre","job":"Software Test Engineer I","salary":"$4.06","location":"Colombia"},
{"id":7,"first_name":"Nevil","last_name":"Matussevich","job":"Safety Technician I","salary":"$3.83","location":"Poland"},
{"id":8,"first_name":"Ivonne","last_name":"Lamping","job":"Analog Circuit Design manager","salary":"$3.07","location":"China"},
{"id":9,"first_name":"Sigvard","last_name":"Linggood","job":"Dental Hygienist","salary":"$5.34","location":"Russia"},
{"id":10,"first_name":"Edna","last_name":"Mundwell","job":"Biostatistician III","salary":"$9.28","location":"United States"},
{"id":11,"first_name":"Cathyleen","last_name":"Conklin","job":"VP Quality Control","salary":"$5.28","location":"Brazil"},
{"id":12,"first_name":"Stephani","last_name":"Trase","job":"Editor","salary":"$8.49","location":"Indonesia"},
{"id":13,"first_name":"Enoch","last_name":"Buddles","job":"Pharmacist","salary":"$3.63","location":"Peru"},
{"id":14,"first_name":"Jennilee","last_name":"Watsham","job":"Programmer Analyst I","salary":"$9.24","location":"Indonesia"},
{"id":15,"first_name":"Tadeas","last_name":"Caslake","job":"Research Nurse","salary":"$4.18","location":"Macedonia"},
{"id":16,"first_name":"Joell","last_name":"Snasdell","job":"Librarian","salary":"$6.62","location":"Croatia"},
{"id":17,"first_name":"Ariana","last_name":"McGillacoell","job":"Chief Design Engineer","salary":"$6.56","location":"China"},
{"id":18,"first_name":"Doro","last_name":"Perrinchief","job":"VP Sales","salary":"$0.89","location":"Zambia"},
{"id":19,"first_name":"Con","last_name":"Basso","job":"Automation Specialist I","salary":"$4.18","location":"China"},
{"id":20,"first_name":"Trixy","last_name":"Ayliffe","job":"Compensation Analyst","salary":"$0.49","location":"South Africa"},
{"id":21,"first_name":"Luelle","last_name":"Puffett","job":"Executive Secretary","salary":"$8.25","location":"China"},
{"id":22,"first_name":"Sylvester","last_name":"Novic","job":"Office Assistant I","salary":"$1.65","location":"South Korea"},
{"id":23,"first_name":"Harmon","last_name":"Pilch","job":"Structural Analysis Engineer","salary":"$0.48","location":"China"},
{"id":24,"first_name":"Nollie","last_name":"Cannavan","job":"Database Administrator I","salary":"$4.21","location":"Poland"},
{"id":25,"first_name":"Elena","last_name":"Asquez","job":"Software Test Engineer III","salary":"$9.42","location":"Azerbaijan"},
{"id":26,"first_name":"Scotty","last_name":"Fearnside","job":"Assistant Manager","salary":"$2.80","location":"Russia"},
{"id":27,"first_name":"Xaviera","last_name":"Wickrath","job":"Administrative Assistant I","salary":"$6.62","location":"Thailand"},
{"id":28,"first_name":"Basile","last_name":"Luna","job":"Data Coordiator","salary":"$2.61","location":"Chile"},
{"id":29,"first_name":"Fayth","last_name":"Lindop","job":"Health Coach II","salary":"$5.14","location":"Indonesia"},
{"id":30,"first_name":"Curtice","last_name":"Mea","job":"Senior Financial Analyst","salary":"$5.00","location":"Colombia"}]





// 1. Find all Web Developers

let webDevelopers = persons.filter((person) => {
    return person.job === "Web Developer II" || person.job === "Web Developer III";
});

console.log("Web Developers:");
console.log(webDevelopers);
console.log('\n');


// 2. Convert all the salary values into proper numbers instead of strings 

let newSalary = persons.map((person) => {
    return parseFloat(person.salary.substring(1));
});

console.log("Salary in numbers:");
console.log(newSalary);
console.log('\n');

// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

persons.map((person) => {
    let correctedSalary = parseFloat(person.salary.substring(1))*10000;
    person.corrected_salary = correctedSalary;
});

console.log("Corrected Salaries:");
console.log(persons);
console.log('\n');

// 4. Find the sum of all salaries 

let sumOfSalaries = persons.reduce(function(acc,curr){
    let newAmount = parseFloat(curr.salary.substring(1));
    acc += newAmount;
    return acc;
},0.0);

console.log("Sum of all salaries:");
console.log(sumOfSalaries);
console.log('\n');

// 5. Find the sum of all salaries based on country using only HOF method

let countryBasedSalarySum = persons.reduce(function(acc,curr){
    if(acc[curr.location] === undefined){
        acc[curr.location] = parseFloat(curr.salary.substring(1));
    } else {
        acc[curr.location] += parseFloat(curr.salary.substring(1));
    }
    return acc;
},{});

console.log("Country based salary sum:");
console.log(countryBasedSalarySum);
console.log('\n');

// 6. Find the average salary of based on country using only HOF method

let avgSalary = persons.reduce(function(acc,curr){
    acc[curr.location] = persons.reduce(function(acc1,curr1){
        if(curr1.location === curr.location){
            acc1 += parseFloat(curr1.salary.substring(1));
        }
        return acc1;
    },0.0)/
    persons.reduce(function(acc2,curr2){
        if(curr2.location === curr.location){
            acc2 += 1;
        }
        return acc2;
    },0);
    return acc;
},{});

console.log("Country wise average salary:");
console.log(avgSalary);